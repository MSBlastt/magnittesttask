<?xml version="1.0" encoding="UTF-8" ?>
<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    version="1.0">
<xsl:output method="xml" omit-xml-declaration="yes" encoding="UTF-8" 
    indent="yes" />
<xsl:strip-space elements="*"/>
  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template match="@*|node()">
    <xsl:copy>
        <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="entry">
    <xsl:copy>
        <xsl:attribute name="field">
          <xsl:value-of select="field"/>
        </xsl:attribute> 
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="field"/>
</xsl:transform>