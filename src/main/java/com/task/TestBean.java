package com.task;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class TestBean implements java.io.Serializable {

    private String dbLogin;
    private String dbPassword;
    private String dbConnectionUrl;
    private int n;

    public TestBean() {
    }

    public static void main(String[] args) {

        //Creating testBean, setting db properties
        TestBean testBean = new TestBean();
        testBean.setN(500);
        testBean.setDbConnectionUrl("jdbc:postgresql://localhost:5432");
        testBean.setDbLogin("postgres");
        testBean.setDbPassword("root");

        System.out.println("-------- Magnit Test Task ------------");

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Unable to find PostgreSQL JDBC Driver! Check Maven dependency!");
            e.printStackTrace();
            return;
        }

        System.out.println("PostgreSQL JDBC Driver Registered!");

        Connection connection;

        try {
            connection = DriverManager.getConnection(
                    testBean.getDbConnectionUrl(), testBean.getDbLogin(), testBean.getDbPassword());
        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return;
        }

        if (connection != null) {
            System.out.println("Connection received! Database is under control now!");
        } else {
            System.out.println("Failed to make connection! Check connection properties!");
        }

        Map<Integer, Integer> rows = null;

        try {

            if (testBean.isTableEmpty(connection)) {
                System.out.println("The table is empty. Porceeding...");
            }
            else {
                System.out.println("The table is not empty. Deleting all rows...");
                testBean.clearTable(connection);
                System.out.println("The table was cleared!");
            }

            System.out.println("Starting filling database with " + testBean.getN() + " rows!");
            testBean.fillDatabaseNtimes(connection, testBean.getN());
            System.out.println("Database was successfully filled!");

            System.out.println("Retrieving all rows for XML bulding...");
            rows = testBean.retrieveAllRows(connection);
            if (rows.size() > 0) {
                System.out.println("Rows retrived! There are " + rows.size() + " rows was received.");
            }
            else {
                System.out.println("There's no data in database! Something wrong!");
                return;
            }

        } catch (SQLException e) {
            System.out.println("Error was caught during querying to database!");
            e.printStackTrace();
        }

        System.out.println("Building XML file...");

        try {
            String xmlBody = testBean.generateXmlBody(rows);
            PrintWriter writer = new PrintWriter("src/main/resources/1.xml", "UTF-8");
            writer.println(xmlBody);
            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            System.out.println("Can't access resource folder!");
            e.printStackTrace();
            return;
        }

        System.out.println("XML file was successfully built!");

        System.out.println("Transforming XML with XSLT processor...");

        TransformerFactory factory = TransformerFactory.newInstance();
        Source xslt = new StreamSource(new File("src/main/resources/xslt.xsl"));
        try {
            Transformer transformer = factory.newTransformer(xslt);
            Source text = new StreamSource(new File("src/main/resources/1.xml"));
            transformer.transform(text, new StreamResult(new File("src/main/resources/2.xml")));
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        System.out.println("XML was successfully transformed!");

        System.out.println("Reading new XML file...");

        StringBuilder sb = new StringBuilder();

        try {
          Files.lines(Paths.get("src/main/resources/2.xml")).forEach(sb::append);
        } catch (IOException e) {
            System.out.println("Error reading 2.xml");
            e.printStackTrace();
        }

        System.out.println("New XML file was read successfully!");

        System.out.println("Starting parse new XML file and getting arithmetic sum...");

        int arithmeticSum = testBean.getArithmeticsum(sb.toString());

        System.out.println("Parsed successfully! Arithmetic sum is " + arithmeticSum);
    }

    private boolean isTableEmpty (Connection connection) throws SQLException {
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM public.test");
        return !rs.next();
    }

    private void clearTable(Connection connection) throws SQLException {
        Statement st = connection.createStatement();
        st.executeUpdate("DELETE FROM public.test");
    }

    private void fillDatabaseNtimes(Connection connection, int n) throws SQLException {
        Statement st = connection.createStatement();
        for (int i = 1; i <= n; i++) {
            st.executeUpdate("INSERT INTO public.test VALUES ("+ i + "," + i + ")");
        }
    }

    private Map<Integer, Integer> retrieveAllRows(Connection connection) throws SQLException {
        Map<Integer, Integer> result = new HashMap<>();
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM public.test");
        while (rs.next()) {
            result.put(rs.getInt("id"),rs.getInt("field"));
        }
        return result;
    }

    private String generateXmlBody(Map<Integer, Integer> rows) {
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\"?>");
        sb.append(getNewLine());
        sb.append("<entries>");
        sb.append(getNewLine());
        for (int n : rows.values()) {
            sb.append("    <entry>");
            sb.append(getNewLine());
            sb.append("        <field>").append(n).append("</field>");
            sb.append(getNewLine());
            sb.append("    </entry>");
            sb.append(getNewLine());
        }
        sb.append("</entries>");
        return sb.toString();
    }

    private int getArithmeticsum(String xml) {
        int result = 0;
        String[] lines = xml.split("><");
        for (String s : lines) {
            if (s.trim().startsWith("entry field=")) {
                result += Integer.parseInt(s.replaceAll( "[^\\d]", "" ));
            }
        }

        return result;
    }

    private static String getNewLine() {
        return System.getProperty("line.separator");
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public String getDbLogin() {
        return dbLogin;
    }

    public void setDbLogin(String dbLogin) {
        this.dbLogin = dbLogin;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

    public String getDbConnectionUrl() {
        return dbConnectionUrl;
    }

    public void setDbConnectionUrl(String dbConnectionUrl) {
        this.dbConnectionUrl = dbConnectionUrl;
    }
}
