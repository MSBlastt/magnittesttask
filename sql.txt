CREATE TABLE public.test
(
    id SERIAL PRIMARY KEY NOT NULL,
    field INT NOT NULL
);
ALTER TABLE public.test
 ADD CONSTRAINT unique_id UNIQUE (id);